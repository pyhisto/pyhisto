#!/usr/bin/env python

#######################
# Libraries/modules
import os
import sys
import signal
import subprocess
import shutil
import pyfits
import numpy
import gamin
import time
import datetime
import argparse
import util
from util import *
#######################

#######################
# Global variables - you can modify these via command-line args

# sleep timer (in seconds) - how often the script checks for 
# new files
SLEEP_TIMER = 10

# directory where camera stores all-sky images
# (i.e. directory which should be scanned for new files;
#  new date-named folders are also created there by the 
#  script)
SBIG_DIRECTORY = '/home/SBIG AS-340/AllSkyImages'

# output directory (new folders and files are created there)
# default is SBIG_DIRECTORY
global OUTPUT_DIRECTORY

# control the camera app within the script
# if this is true, camera app will be started along with the
# script and it will begin capturing images. If the script
# gets interrupted it will tell camera to stop capturing and
# close the camera app
CAMERA_CONTROL = True

# FITS mask file path
FITS_MASK_PATH = SBIG_DIRECTORY + '/mask.fits'

# intensity range over one bin in output histogram
# (span of ADUs in each bin;
#  bin count = 65536 / INTENSITY_RANGE)
INTENSITY_RANGE = 2.0

# histogram data file extension
EXT_DATA = '.hist'

# plot file extension
EXT_PLOT = '.ps'

# use camera time for timestamps - highly recommended
# otherwise, system time is used, but this can be inaccurate
# (see make_timestamp func for more info)
USE_CAMERA_TIME = True

# do NOT apply mask flag
SKIP_MASK = False

# save masked FITS
SAVE_MASKED_FITS = False

# FITS mask
global FITS_MASK

# main Gamin monitor
global Mon

#######################


#######################
# Global constants - do NOT modify these

# Gamin event enums
GAMIN_EVTS = { 1 : 'Changed',
               2 : 'Deleted',
               3 : 'StartExecuting',
               4 : 'StopExecuting',
               5 : 'Created',
               6 : 'Moved',
               7 : 'Acknowledge',
               8 : 'Exists',
               9 : 'EndExists' }

# number of pixel bins in output histogram (sampling rate)
NBINS = 65536.0 / INTENSITY_RANGE

# the largest index of non-zero array element (for plotting range)
global MAX_INDEX

# last directory time stamp
global LAST_DIR_TIMESTAMP

# last file time stamp
global LAST_FILE_TIMESTAMP

# run main loop flag
global RUN_LOOP

#######################

def find_max_index( array ):
    '''Finds the biggest index of non-zero elements in the input array.'''

    index = 0
    last = 0
    for i in enumerate( array ):
        if last != 0 and i[1] == 0:
            index = i[0]
        last = i[1]
        
    return index

def parse_fits( input_file, output_file ):
    '''Opens a FITS input_file and saves its histogram to output_file.'''

    # Open input_file
    hdulist = pyfits.open( input_file )

    debug_print( 'Filename: ' + input_file )
    debug_print( 'Timestamp: ' + hdulist[0].header['DATE-OBS'] )
    debug_print( 'Exposition: %e' % ( hdulist[0].header['EXPTIME'] ) )

    # Get exposure time from the header
    exp_time = float( hdulist[0].header['EXPTIME'] )

    # If exposure is less than 10 seconds (check this value)
    # skip histogram stuff
    if exp_time < 10.: return False

    # Mask out bad pixels and save it
    if not SKIP_MASK:
        debug_print( 'Masking out bad pixels... ', False )

        hdulist[0].data = numpy.multiply( hdulist[0].data, FITS_MASK[0].data )
        if SAVE_MASKED_FITS:
            hdulist.writeto( input_file.rpartition( '.' )[0] + '.m.FIT' )

        debug_print( 'Done.' )

    scidata = hdulist[0].data

    debug_print( 'Creating and saving histogram... ', False )
    # Create histogram array from pixel data
    if exp_time > 1.:
        histogram, bin_edges = numpy.histogram( scidata / exp_time, NBINS, ( 0.0, 65535.0 ) )
    else:
        histogram, bin_edges = numpy.histogram( scidata, NBINS, ( 0.0, 65535.0 ) )

    # Find the maximum index
    global MAX_INDEX
    MAX_INDEX = find_max_index( histogram )

    # Print the parameters
    # param( histogram )

    # Write histogram data to the output file
    # output file format:
    # BIN_INDEX INTENSITY_UPPER_LIMIT PIXEL_COUNT
    f = file( output_file, 'w' )
    for i in enumerate( histogram ):
        f.write( str( i[0] ) + ' ' + str( (i[0] + 1) * INTENSITY_RANGE ) + ' ' + str( i[1] ) + '\n' )
        if i[0] == MAX_INDEX:
            break

    # Close input files
    f.close()
    hdulist.close()
    debug_print( 'Done.' )
    return True

def plot_histo( input_file, output_file ):
    '''Plots histogram data from output_file using gnuplot.'''
            
    max_intensity = (MAX_INDEX + 1) * INTENSITY_RANGE
    ticks = max_intensity / 5

    debug_print( 'Plotting histogram... ', False )

    # Plotting script, passed to gnuplot
    PLOT_SCRIPT = '''
gnuplot << EOF
	set terminal postscript enhanced solid lw 1.5
	set out "%s"
	unset label
	unset key
        set tics out
        set ytics nomirror
	set xlabel "Intensity"
	set ylabel "Pixel count"
	set title "Histogram of %s (bins: %d)"
        set xtics %d mirror rotate
        set pointsize 0.8
	plot [0:%d] "%s" using 2:3 with boxes
EOF
''' % ( LAST_DIR_TSTAMP + '/HGRAMS/' + os.path.basename( input_file ) + EXT_PLOT, 
        input_file, NBINS, int( ticks ), int( max_intensity ), output_file )

    # Run gnuplot (gnuplot must be in $PATH)
    shell( PLOT_SCRIPT )

    debug_print( 'Done.' )
    debug_print( 'Max index (intesity): %d (%f)' % (MAX_INDEX, max_intensity) )

def param( array ):
    '''Calculates various parameters.'''

    # theta - the largest positive gradient between two points
    #         3 pixel values apart
    last = 0
    theta = ( 0, 0 )
    for i in range( 0, MAX_INDEX, 3 ):
        if array[i] - last > theta[0]:
            theta = ( array[i] - last, i )
        last = array[i]

    print 'Theta = ', theta

    # alpha - ration of pixel count at the mid-point value of
    #         the histogram and pixel count at theta
    alpha = float( array[MAX_INDEX / 2] ) / float( theta[0] )
    
    print 'Alpha = ', alpha

    # max_count - the point of maximum pixel count
    max_count = ( array.max(), array.argmax() )
    
    # beta - difference in pixel values between the point of maximum gradient
    #        (theta) and the point of maximum counts
    beta = max_count[1] - theta[1]

    print 'Beta = ', beta
    
    
    # gamma - ratio between the count at the point of maximum counts
    #         and the point of maximum gradient (theta)
    gamma = float( max_count[0] ) / float( theta[0] )

    print 'Gamma = ', gamma

    # epsilon - sum of deviations of each pixel value from a line drawn
    #           between the top of the point of maximum gradient (theta)
    #           and zero at pixel value 105/127 * MAX_INDEX
    
    # Relative point
    rel = ( array[105.0/127.0 * MAX_INDEX], int( 105.0/127.0 * MAX_INDEX ) )

    # Line eqn. coefficient and constant
    coeff = (theta[0] - rel[0]) / (theta[1] - rel[1])
    const = - coeff * rel[1] + rel[0]
    
    epsilon = 0
    for i in range( theta[1], rel[1] ):
        # maybe abs?
        calculated = coeff * i + const
        epsilon += calculated - array[i]
        
    print 'Epsilon = ', epsilon

    # phi - sum of the differences of the counts of a pixel value and that
    #       of 2 pixel values higher, taken over the range of 50/127 * MAX_INDEX
    #       pixel values after the point of maximum gradient (theta)
    phi = 0
    last = theta[0]
    for i in range( theta[1] + 2, int( 50.0/127.0 * MAX_INDEX ), 2 ):
        # maybe abs?
        phi += last - array[i]
        last = array[i]
        
    print 'Phi = ', phi

    print 'Sum = ', 1.2 * (2 * alpha**2 + beta / 16.0 + 2.5 * gamma + epsilon - phi)

def make_timestamp( use_camera_time = True, fits_filename = '' ):
    if use_camera_time:
        # Obtain time/date observed from the FITS header
        hdulist = pyfits.open( fits_filename )
        obs = hdulist[0].header['DATE-OBS']
        hdulist.close()
        
        dt = datetime.datetime.strptime( obs, '%Y-%m-%dT%H:%M:%S.%f' )
        # Translate camera time to localtime (idk why but it's set to GMT-1)
        real_dt = dt + datetime.timedelta( hours = 2 )
    else:
        # Use with care! Script may lag a bit behind camera app because of image
        # processing. Thus, time obtained at the moment script notices there's a new
        # FITS file to process may differ a lot from the time that image was shot at.
        real_dt = datetime.datetime.now()

    hour = real_dt.hour
    mnt = real_dt.minute
    sec = real_dt.second

    # Check if the time is < 6 AM, in that case we put files in the directory
    # created yesterday (last night).
    death = datetime.time( 6, 0 )
    if real_dt.time() < death:
        real_dt = real_dt - datetime.timedelta( 1 )

    date = '%d/%.2d/%.2d' % (real_dt.year, real_dt.month, real_dt.day)
    return "%.2d-%.2d-%.2d" % (hour, mnt, sec), "%s/%s" % (OUTPUT_DIRECTORY, date)

def gamin_callback( path, event ):
    '''Gets called whenever folder that is being watched gets modified.
    
    Gamin server generates a corresponding event every time something in
    the watched folder changes. These include: file changed/deleted/created
    or moved event, program executed event and program stopped event. Also, 
    at the beginning of monitoring, a series of exists events are generated.
    These are used to notify about existing files in that folder.

    See GAMIN_EVTS dictionary.'''

    debug_print( 'Got callback: %s (%s)' % (path, GAMIN_EVTS[event]) )
        
    # Check if this is Created or Exists event
    if 5 == event or 8 == event:
        # Get file extension and name
        ext = path.rpartition( '.' )[2]
        fname = path.rpartition( '.' )[0]
        # We are not interested in current images nor other file types than FIT
        if fname == 'AllSkyCurrentImage' or ext != 'FIT':
            return

        full_path = SBIG_DIRECTORY + '/' + path

        if not util.DEBUG:
            # This should be the only line for each file
            # printed when not in debug mode
            print full_path

        # Get current timestamp and dirname based on system or camera time
        if 8 == event:
            # Always use camera time for FITs that were already there
            timestamp, dir = make_timestamp( True, full_path )
        else:
            timestamp, dir = make_timestamp( USE_CAMERA_TIME, full_path )

        # Assert that this directory exist
        # NB: the script will create ONE set of directories for every night
        # it's run. For example, if it was run at 18:00 PM 22/10/2011,
        # it will create these directories:
        # OUTPUT_DIRECTORY/2011/10/22/{JPEG,FITS,HGRAMS}
        # and it will move all the files to those directories as long as it
        # isn't interrupted (6:00 AM 23/10/2011 or Ctrl-C).
        if not os.path.exists( dir ):
            debug_print( 'Creating directories (' + dir + ')' )
            os.makedirs( dir + '/FITS' )
            os.makedirs( dir + '/JPEGS' )
            os.makedirs( dir + '/HGRAMS' )

        # Move file and its JPEG sidekick to their corresponding folders
        shutil.move( SBIG_DIRECTORY + '/' + path, dir + '/FITS/' + path )
        if os.path.exists( SBIG_DIRECTORY + '/' + fname + '.JPG' ):
            shutil.move( SBIG_DIRECTORY + '/' + fname + '.JPG', dir + '/JPEGS/' + fname + '.JPG' )

        global LAST_DIR_TSTAMP
        LAST_DIR_TSTAMP = dir

        # Create histogram for this file
        if parse_fits( dir + '/FITS/' + path, dir + '/HGRAMS/' + fname + EXT_DATA ):
            plot_histo( dir + '/FITS/' + path, dir + '/HGRAMS/' + fname + EXT_DATA )
    
def shut_down():
    global Mon
    global RUN_LOOP

    if CAMERA_CONTROL:
        # Stop camera app
        shell( 'winpython27 camera_control.py stop' )

    Mon.stop_watch( SBIG_DIRECTORY )
    RUN_LOOP = False

def signal_handler( signal, frame ):
    '''This handler gets called whenever SIGINT is generated.
    
    A SIGINT can be generated by pressing Ctrl-C in the shell or
    by sending SIGINT signal via `kill -s SIGINT pid`.
    
    That way we can stop the script in a laid-back manner. It will
    finish what it was doing, release all resources used and exit.'''


    print 'Caught SIGINT, exiting gracefully...'
    shut_down()

def parse_args():
    '''Parses command-line arguments.'''

    # Create and populate the parser
    parser = argparse.ArgumentParser( description = 'Capture and process all-sky images.', 
                                      epilog = 'Report bugs to templaryum@gmail.com' )
    parser.add_argument( '-s', '--sleep-timer', type = int,
                         help = 'How often the script should check for directory changes. (default: 10 secs)',
                         metavar = 'SECONDS' )
    parser.add_argument( '-D', '--disable-camera-ctrl', action = 'store_true',
                         help = 'Disable camera control: script automatically starts camera app and starts shooting by default.' )
    parser.add_argument( '-o', '--output-dir', help = 'Directory where images will be moved and sorted within new directories. '
                         '(default: INPUT_DIR)' )
    parser.add_argument( '-m', '--mask', help = 'Path to FITS mask file. (default: INPUT_DIR/mask.fits)' )
    parser.add_argument( '-r', '--intensity-range', type = float, help = 'ADU span over one histogram bin. (default: 2.0)' )
    parser.add_argument( '--use-system-time', action = 'store_true', 
                         help = 'Use system time instead of camera time for timestamps (discouraged).' )
    parser.add_argument( '-v', '--verbose', action = 'store_true',
                         help = 'Verbose output.' )
    parser.add_argument( '-k', '--skip-mask', help = 'Skips masking step.', action = 'store_true' )
    parser.add_argument( 'INPUT_DIR', help = 'Directory where all-sky images are stored.' )

    # Parse arguments and assign values to global variables
    args = parser.parse_args()
    
    global SLEEP_TIMER
    global SBIG_DIRECTORY
    global OUTPUT_DIRECTORY
    global CAMERA_CONTROL
    global FITS_MASK_PATH
    global INTENSITY_RANGE
    global USE_CAMERA_TIME
    global SKIP_MASK
    global NBINS

    if not os.path.exists( args.INPUT_DIR ):
        print 'Error! Input directory does not exist.'
        sys.exit( 1 )

    SBIG_DIRECTORY = args.INPUT_DIR

    if args.sleep_timer:
        if args.sleep_timer < 0:
            print 'Error! Sleep timer less than 0.'
            sys.exit( 1 )
        SLEEP_TIMER = args.sleep_timer
    if args.disable_camera_ctrl:
        CAMERA_CONTROL = False
    if args.output_dir:
        # should be created later even if it doesn't exist
        OUTPUT_DIRECTORY = args.output_dir
    else:
        OUTPUT_DIRECTORY = SBIG_DIRECTORY
    if args.mask:
        FITS_MASK_PATH = args.mask
    if args.intensity_range:
        if args.intensity_range < 0 or args.intensity_range > 65536.0:
            print 'Error! Wrong intensity range.'
            sys.exit( 1 )
        INTENSITY_RANGE = args.intensity_range
        NBINS = 65536.0 / INTENSITY_RANGE
    if args.use_system_time:
        USE_CAMERA_TIME = False
    if args.verbose:
        util.DEBUG = True
    if args.skip_mask:
        SKIP_MASK = True

    debug_print( 'Parameters:\n'
                 '--------------------\n'
                 '* INPUT_DIR   = %s\n'
                 '* OUTPUT_DIR  = %s\n'
                 '* SLEEP_TIMER = %d\n'
                 '* CAMERA_CTRL = %r\n'
                 '* FITS_MASK   = %s\n'
                 '* SKIP_MASK   = %r\n'
                 '* INT_RNG     = %.3f (%d bins)\n'
                 '* USE_CAM_TM  = %r\n'
                 % (SBIG_DIRECTORY, OUTPUT_DIRECTORY, SLEEP_TIMER,
                    CAMERA_CONTROL, FITS_MASK_PATH, SKIP_MASK, INTENSITY_RANGE, 
                    NBINS, USE_CAMERA_TIME) )

# This line checks if this script is being run explicitely, or
# it was included by some other script as a module
if __name__ == '__main__':
    # Record start time
    start_time = datetime.datetime.now()
    tomorrow = start_time + datetime.timedelta( 1 )

    # Parse command line arguments
    parse_args()

    # Set up signal catcher
    signal.signal( signal.SIGINT, signal_handler )

    # Set up the Gamin monitor
    global Mon
    try:
        Mon = gamin.WatchMonitor()
        Mon.watch_directory( SBIG_DIRECTORY, gamin_callback )
    except gamin.GaminException:
        print 'Error! Could not establish connection to Gamin server.'
        sys.exit( 1 )

    # Wait up for events to generate
    time.sleep( 1 )

    # Open the fits mask
    global FITS_MASK
    if not SKIP_MASK:
        try:
            FITS_MASK = pyfits.open( FITS_MASK_PATH )
        except IOError:
            print 'Error! FITS mask file cannot be opened.'
            sys.exit( 1 )

    if CAMERA_CONTROL:
        # Start up camera app and start capturing
        shell( 'winpython27 camera_control.py start' )
    
    # Main loop
    global RUN_LOOP
    RUN_LOOP = True;
    while True:
        while Mon.event_pending() > 0:
            # Handle registered event
            Mon.handle_one_event()

            # Check if it's time to die (6 AM)
            now = datetime.datetime.now()
            death = datetime.datetime.combine( tomorrow.date(), datetime.time( 6, 0 ) )
            if death < now:
                debug_print( 'All those moments will be lost in time, like tears in rain. '
                             'Time to die.' )
                shut_down()
                break

        if not RUN_LOOP:
            break
        time.sleep( SLEEP_TIMER )

    if Mon.event_pending() > 0:
        debug_print( "Handling remaining events (shouldn't be any, but wth)..." )
        Mon.handle_events()

    del Mon
    if not SKIP_MASK:
        FITS_MASK.close()
