# She-bang line deliberately left out.

# This script is meant to be run from the main script using Windows
# Python 2.7 interpreter, since some pywinauto objects we need are
# not accessible from within Cygwin implementation

# Usage: winpython27 camera_control.py start|stop

import pywinauto
import time
import sys
from util import debug_print

APP_PATH = r'C:\Program Files\SBIG\SBIG AllSky-340\SBIG All Sky Camera.exe'

def start_app():
    '''Starts the camera app. Returns app handle.'''
    
    debug_print( 'Starting camera application... ', False )
    try:
        app = pywinauto.application.Application().start_( APP_PATH )
    except pywinauto.application.AppStartError, x:
        print 'Could not start application:', x.message
        sys.exit( 1 )
    except:
        print 'Unexpected error:', sys.exc_info()[0]
        sys.exit( 1 )

    debug_print( 'Done.' )
    return app

def connect_app():
    '''Connects to running camera app. Returns app handle.'''

    debug_print( 'Connecting to camera app... ', False )
    app = pywinauto.application.Application()
    try:
        app.connect_( path = APP_PATH )
    except pywinauto.application.ProcessNotFoundError, x:
        print 'Could not find process:', x.message
        sys.exit( 1 )
    except:
        print 'Unexpected error:', sys.exc_info()[0]
        sys.exit( 1 )

    debug_print( 'Done.' )
    return app

def press_capture_btn( app ):
    '''Presses capture button using application handle app.

    Used to start or stop capturing.'''

    debug_print( "Pressing capture button... ", False )
    # This should throw no exceptions
    try:
        app['SBIG AllSky-340'].Button3.Click()
    except:
        print 'Unexpected error:', sys.exc_info()[0]
        exit( 1 )

    debug_print( "Done." )

def close_app( app ):
    '''Closes camera app using application handle app.'''

    debug_print( 'Closing the app...', False )
    try:
        app['SBIG AllSky-340'].Close()
    except pywinauto.timings.TimeoutError:
        # Safe to ignore
        pass

    debug_print( 'Done.' )

if __name__ == '__main__':
    # Check if command line args were supplied correctly
    if 2 != len( sys.argv ):
        print 'Error! Feed me with exactly 1 argument.'
        sys.exit( 1 )
	
    if 'start' == sys.argv[1]:
        app = start_app()

      	# Wait for it to settle down
        time.sleep( 5 )

        press_capture_btn( app )
        del app
    elif 'stop' == sys.argv[1]:
        app = connect_app()
        press_capture_btn( app )
        
      	# Wait for it to settle down
        time.sleep( 5 )
		
        close_app( app )
        del app
    else:
        print 'Error! Unknown argument:', sys.argv[1]
        sys.exit( 1 )
                
