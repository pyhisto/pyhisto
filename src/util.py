# She-bang line deliberately left out.

# This module contains some utility functions used by other scripts.

import subprocess
import sys

DEBUG = False

def debug_print( formatted_string, new_line = True ):
    '''Prints formatted_string to stdout, with or without a newline.'''

    if DEBUG:
        if new_line:
            print formatted_string
        else:
            print formatted_string,
        sys.stdout.flush()

def shell( cmd ):
    '''Pipes to shell and executes supplied command.

    Returns stdout output of that command.'''

    p = subprocess.Popen( cmd, shell=True, stdout=subprocess.PIPE )
    out = p.stdout.read().strip()
    # debug_print( out )
    return out
